import edu.cmu.sphinx.api.Configuration;

public class SpeachRecognition {
    Configuration configuration;

    public SpeachRecognition(){
        configuration = new Configuration();

        // Set path to acoustic model.
        configuration.setAcousticModelPath("resource:/");
        // Set path to dictionary.
        configuration.setDictionaryPath("resource:/frenchWords62K.dic");
        // Set language model.
        configuration.setLanguageModelPath("resource:/french3g62K.lm.dmp");
    }

}
