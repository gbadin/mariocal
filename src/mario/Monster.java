package mario;

import java.awt.*;

public class Monster extends Rectangle{
	public String nom;
	public double x,y,height,width;
	public double dx,dy;
	public Image img;
	public boolean ToucheLeSol, mort;
	public boolean sens;
	public Rectangle R;


	public Monster(String param1,Image param2, int param3, int param4)
	{
		nom=param1;
		x=param3;
		y=param4;
		height=50;
		width=40;
		dx=0;
		dy=0;
		ToucheLeSol=false;
		sens=true;
		img=param2;
		
	}
	
	public void Dessiner(Graphics g)
	{
		g.drawImage(img,(int)x,(int)y,40,50, null);
	}
	
	public void Bouger(int para1, int para2)
	{
		y=y+para2;
		dx=3;
		if(sens==true)
		{
			x=x+para1+dx;
		}
		else if(sens==false)
		{
			x=x+para1-dx;
		}
		
	}
	
	
	public boolean Collision(Rectangle r){
		R= new Rectangle((int)x,(int)y,40,50);
		if(R.intersects(r))
		{
			return true;
		}
		else{
			return false;
		}
	}
	
	public void Meurt(){
		if(y>=1000 || y<-100){
			mort=true;
			dx=0;
			dy=0;
		}
	}
	
	public void efface(){
		if(y>=800 || y<-100){
			img=null;
		}
	}

}