package mario;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.lang.model.element.VariableElement;
import javax.swing.JFrame;


public class MarioLike extends JFrame implements KeyListener, Runnable{
	Image fond,fondPerdu, imgBonus;
	Image imgMonster, imgMonster2;
	Image imgJoueur1,imgJoueur2,imgJoueur3,imgJoueur4;
	Image imgDecor,imgDecor2;
	Joueur j1;
	Bonus b1;
	Layout l1;
	boolean PressGauche, PressDroit, PressHaut, PressBas, PressSpace;

	Rectangle R1;
	int vargrav;
	Decor[] Decor1;
	Decor[] Decor2;
	
	private Image doubleBuffer;
	private Graphics gBuffer;
	private Thread processusJeu;
	private URL url1;
	private Decor [][] quadriDecor;
	private Monster [][] quadriMonster;
	private Bonus [][] quadriBonus;
	private int nbLignes,nbCol;
	
	
	
	public MarioLike()
	{
		super("Mario 2011");
		//Fond
		java.awt.Toolkit toolkit=java.awt.Toolkit.getDefaultToolkit();
		fond=toolkit.getImage("fond.jpg");
		fondPerdu=toolkit.getImage("fondPerdu.jpg");
		setSize(1280,720);
		setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.addKeyListener(this);
		PressGauche=false;
		PressDroit=false;
		PressHaut=false;
		PressBas=false;
		vargrav=1;
		
		//Mario
		imgJoueur1=toolkit.getImage("marioD.png");
		imgJoueur2=toolkit.getImage("marioG.png");
		imgJoueur3=toolkit.getImage("MarioDRetourne.png");
		imgJoueur4=toolkit.getImage("MarioGRetourne.png");
		j1= new Joueur("Bob",imgJoueur1);
		
		//Bonus
		imgBonus=toolkit.getImage("pomme.png");
		b1= new Bonus("Champi",imgBonus,100,100);
		
		//Monster
		imgMonster=toolkit.getImage("Goomba.png");
		imgMonster2=toolkit.getImage("GoombaRetourne.png");
		//m1= new Monster("Monster",imgMonster,200,200);
		
		//Decor
		imgDecor=toolkit.getImage("sol3.png");
		
		l1=new Layout(1,0,0);
	
		nbLignes=12;
		nbCol=200;
		quadriDecor=new Decor[nbLignes][nbCol];
		quadriMonster=new Monster[nbLignes][nbCol];
		quadriBonus=new Bonus[nbLignes][nbCol];

		chargeLeNiveau(1);
		
		processusJeu=new Thread(this);
		processusJeu.start();
        doubleBuffer = createImage(1280,720);
		gBuffer=doubleBuffer.getGraphics();
	}
	
	
	
	
	public static void main(String[]args)
	{
        new MarioLike();
	}

	//chargement de niveau
	 public void chargeLeNiveau(int numNiveau)
	 {
		 l1.level++;
		 String ligne;	  
		 try
		 {
			 BufferedReader tamponFichier= new BufferedReader(new InputStreamReader(new FileInputStream("level"+numNiveau+".txt")));
	         int numLigne=0;
		   while ((ligne=tamponFichier.readLine()) != null)
	         {			 
		     char [] tab=ligne.toCharArray();	 
		     for(int i=0;i<nbCol;i++)
		     {
			if(tab[i]=='#')
			{
			 quadriDecor[numLigne][i]=new Decor("sol",imgDecor, (i * 70),670-((11-numLigne)*60));
			}
			if(tab[i]=='*')
			{
			 quadriMonster[numLigne][i]=new Monster("Monster",imgMonster, (i * 70),685-((11-numLigne)*60));
			}
			if(tab[i]=='$')
			{
			 quadriBonus[numLigne][i]=new Bonus("Champi",imgBonus, (i * 70),685-((11-numLigne)*60));
			}
		   } 
		    System.out.println("Ligne trait�e:"+numLigne);
		    numLigne++;
		}
		 
		 }
		 catch (Exception e){System.out.println("Erreur dans le traitement du fichier niveau "+numNiveau+" : "+e);}	 
	 }
	
	 
	 //affichage
	public void paint(Graphics g)
	{
		super.paint(gBuffer);
		gBuffer.drawImage(fond,0,0,this);
		
		j1.Dessiner(gBuffer);
		b1.Dessiner(gBuffer);
		
		
		for(int i=0;i<nbLignes;i++)
	     {
	    	 for(int j=0;j<nbCol;j++)
	         {
	        	 if(quadriDecor[i][j]!=null) quadriDecor[i][j].Dessiner(gBuffer);
	        	 if(quadriMonster[i][j]!=null) quadriMonster[i][j].Dessiner(gBuffer);
	        	 if(quadriBonus[i][j]!=null) quadriBonus[i][j].Dessiner(gBuffer);
	         }	 
	     } 
		
		l1.Dessiner(gBuffer);
		g.drawImage(doubleBuffer,0,0,this);		
	}
	
	
/*public void keyboard(KeyEvent event){
		int key=event.getKeyCode();
		switch(key)
		{
		case KeyEvent.VK_RIGHT:
			j1.Bouger(5,0); this.repaint(); break;
			
		case KeyEvent.VK_LEFT:  
			j1.Bouger(-5,0);  this.repaint(); break;
		}
		
	}*/

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int key=e.getKeyCode();
		switch(key)
		{
		case KeyEvent.VK_LEFT:
			j1.ToucheLeSol=false;
			if(vargrav==1)
				{ j1.img=imgJoueur2; }
			else
				{ j1.img=imgJoueur4; }
			
			PressGauche=true;
			j1.PeuxTournerADroite=true;
			break;
			
		case KeyEvent.VK_RIGHT:
			j1.ToucheLeSol=false;
			if(vargrav==1)
				{ j1.img=imgJoueur1; }
			else 
				{ j1.img=imgJoueur3; }
			
			PressDroit=true;
			j1.PeuxTournerAGauche=true;
			break;
			
		case KeyEvent.VK_DOWN:
			if(j1.DoubleSaut==0 || j1.DoubleSaut==1){
				j1.dy=30;
				j1.ToucheLeSol=false;
				j1.DoubleSaut++;
			}
			break;
			
			//__touche du Haut /Saut__ \\
		case KeyEvent.VK_UP:
			if(j1.DoubleSaut==0 || j1.DoubleSaut==1){
				j1.dy=-30;
				j1.ToucheLeSol=false;
				j1.DoubleSaut++;
			}
			break;
		
			//__Touche Espace__\\
			// inverse les images en fonction de la gravit� et la direction \\
		case KeyEvent.VK_SPACE:
			if(j1.ToucheLeSol){
				j1.ToucheLeSol=false;
				if(vargrav==1){
					vargrav=vargrav*(-1);
					//m1.img=imgMonster2;
						//changer image monstre\\
						for(int i=0;i<nbLignes;i++){					
							for(int j=0;j<nbCol;j++){
								if(quadriMonster[i][j]!=null){
								quadriMonster[i][j].img=imgMonster2;
								}
							}
						}
					if(j1.img==imgJoueur1)
						{j1.img=imgJoueur3; }
					else if(j1.img==imgJoueur2)
						{j1.img=imgJoueur4;}
					}
					else if(vargrav==-1){
						vargrav=vargrav*(-1);
						//m1.img=imgMonster;
						
						for(int i=0;i<nbLignes;i++){					
							for(int j=0;j<nbCol;j++){
								if(quadriMonster[i][j]!=null){
								quadriMonster[i][j].img=imgMonster;
								}
							}
						}
						if(j1.img==imgJoueur3)
							{ j1.img=imgJoueur1; }
						else
							{ j1.img=imgJoueur2; }
					}
				}
			
		}
	}
	
	public void keyReleased(KeyEvent e){
		// TODO Auto-generated method stub
		int key=e.getKeyCode();
		switch(key)
		{
		case KeyEvent.VK_LEFT:
			PressGauche=false;
			j1.PeuxTournerAGauche=true;
			break;
		case KeyEvent.VK_RIGHT:
			PressDroit=false;
			j1.PeuxTournerADroite=true;
			break;
		case KeyEvent.VK_DOWN:
			PressBas=false;
			break;
		case KeyEvent.VK_UP:
			PressHaut=false;
			break;
		case KeyEvent.VK_SPACE:
			PressSpace=false;
			break;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void update(Graphics g)
	{
		paint(g);
	}
			
//__________________________________________________THREAD___________________________________________________\\
	public void run(){
				while(true)
				{
					try
					{
						//monJeu.evoluer();
						Thread.sleep(10);
						
					//____ bouger tableau monstre ____\\
						//j1.ToucheLeSol=false;
						for(int i=0;i<nbLignes;i++){					
							for(int j=0;j<nbCol;j++){
									if(quadriMonster[i][j]!=null){
										quadriMonster[i][j].Bouger(0, 0);
										quadriMonster[i][j].efface();
									}
							}
						}
						
						//System.out.println(j1.y);
						gravite();
						j1.Meurt();
						perdu();
						l1.vie(j1.nbvies);
						
						
						
						
						//Colision:\\	
						
						for(int i=0;i<nbLignes;i++){					
							for(int j=0;j<nbCol;j++){
								//Collision Sol\\
								
								
					//________________________collision gravit� simple rectangle du bas________________\\
								if(!j1.mort && quadriDecor[i][j]!=null && vargrav>0){
									if(quadriDecor[i][j].Collision(new Rectangle((int)(j1.x+10),(int)(j1.y+55),(int)(j1.width-20),(int)(j1.height-55))))
									{
										j1.ToucheLeSol=true;
										j1.dy=0;
										j1.y=quadriDecor[i][j].y-j1.height;
										j1.DoubleSaut=0;
										
									}
								}
								
								
									//saute et touche un carr�\\
								if(!j1.mort && quadriDecor[i][j]!=null && vargrav>0){
									if(quadriDecor[i][j].Collision(new Rectangle((int)(j1.x+10),(int)(j1.y),(int)(j1.width-20),(int)(j1.height-55))))
									{
										j1.ToucheLeSol=true;
										j1.dy=+1;
										j1.y=quadriDecor[i][j].y+quadriDecor[i][j].height;
									}
								}
								
					//____________________________collision gravit� invers�_____________________________\\
									// pied pass� en haut\\
								if(!j1.mort && quadriDecor[i][j]!=null && vargrav<0){
									if(quadriDecor[i][j].Collision(new Rectangle((int)(j1.x+10),(int)(j1.y),(int)(j1.width-20),(int)j1.height-55)))
									{
										j1.ToucheLeSol=true;
										j1.dy=0;
										j1.y=(quadriDecor[i][j].y+quadriDecor[i][j].height);
										j1.DoubleSaut=0;
									}
								}
								if(!j1.mort && quadriDecor[i][j]!=null && vargrav<0){
									if(quadriDecor[i][j].Collision(new Rectangle((int)(j1.x+10),(int)(j1.y+55),(int)(j1.width-20),(int)j1.height-55)))
									{
										j1.ToucheLeSol=true;
										j1.dy=-1;
										j1.y=(quadriDecor[i][j].y-j1.height);
								
									}
								}
								
					//_____________________ collision entre Mario  et les cot� du sol _________________\\	
								
								// collision sur les cot� gauche \\
								if(!j1.mort && quadriDecor[i][j]!=null){
									if(quadriDecor[i][j].Collision(new Rectangle((int)(j1.x-5),(int)(j1.y+5),2,(int)j1.height-10)))
									{
										j1.PeuxTournerAGauche=false;
										j1.x=quadriDecor[i][j].x+quadriDecor[i][j].width;
									}
									
								}
								// collision sur les cot� droite \\
								if(!j1.mort && quadriDecor[i][j]!=null){
									if(quadriDecor[i][j].Collision(new Rectangle((int)(j1.x+40),(int)(j1.y+5),2,(int)j1.height-10)))
									{
										j1.PeuxTournerADroite=false;
										j1.x=quadriDecor[i][j].x-j1.width;
									}
									
								}
								
		
								
					//_____________________ collision entre les monstres et les cot� du sol _________________\\
								if(quadriDecor[i][j]!=null){
									for(int u=0;u<nbLignes;u++){					
										for(int w=0;w<nbCol;w++){
											if(quadriMonster[u][w]!=null){	
												if(!quadriMonster[u][w].mort && quadriDecor[i][j]!=null){
													if(quadriDecor[i][j].Collision(new Rectangle((int)(quadriMonster[u][w].x-5),(int)(quadriMonster[u][w].y+5),2,(int)quadriMonster[u][w].height-10)))
													{
														quadriMonster[u][w].sens=true;
													}
													
													if(quadriDecor[i][j].Collision(new Rectangle((int)(quadriMonster[u][w].x+41),(int)(quadriMonster[u][w].y+5),2,(int)quadriMonster[u][w].height-10)))
													{
														quadriMonster[u][w].sens=false;

													}
												}
											}
										}
									}
								}
								
								
				//______________________________ collision  tableau monstre sol ______________________________\\
								if(quadriDecor[i][j]!=null){
									for(int u=0;u<nbLignes;u++){					
										for(int w=0;w<nbCol;w++){
											if(quadriMonster[u][w]!=null){	
												if(!quadriMonster[u][w].mort){
													if(vargrav>0){
														if(quadriDecor[i][j].Collision(new Rectangle((int)(quadriMonster[u][w].x),(int)(quadriMonster[u][w].y),(int)(quadriMonster[u][w].width),(int)(quadriMonster[u][w].height))))
														{
															quadriMonster[u][w].ToucheLeSol=true;
															quadriMonster[u][w].dy=0;
															quadriMonster[u][w].y=(quadriDecor[i][j].y)-quadriMonster[u][w].height;
														}
													}
													else if(vargrav<0){
														if(quadriDecor[i][j].Collision(new Rectangle((int)(quadriMonster[u][w].x),(int)(quadriMonster[u][w].y),(int)(quadriMonster[u][w].width),(int)(quadriMonster[u][w].height))))
														{
															quadriMonster[u][w].ToucheLeSol=true;
															quadriMonster[u][w].dy=0;
															quadriMonster[u][w].y=(quadriDecor[i][j].y+quadriDecor[i][j].height);
															
														}
													}
												}
												
											}
										}
									}
									
								}
								
					//_____________________si pas de collision________________________\\	
								
									if(quadriDecor[i][j]!=null){
										for(int u=0;u<nbLignes;u++){					
											for(int w=0;w<nbCol;w++){
												if(quadriMonster[u][w]!=null){	
													if(quadriMonster[u][w]!=null){
														if(!quadriDecor[i][j].Collision(new Rectangle((int)(quadriMonster[u][w].x),(int)(quadriMonster[u][w].y),(int)(quadriMonster[u][w].width),(int)quadriMonster[u][w].height)))
														{
															quadriMonster[u][w].ToucheLeSol=false;
														}
													}
												}
											}
										}
									}
					//____________________________________________________________________\\
					//________________Collision Mario/monstre (saut� dessus)________________\\	
					
									//collision tableau Monstre /joueur\\
									
										for(int u=0;u<nbLignes;u++){					
											for(int w=0;w<nbCol;w++){
												if(quadriMonster[u][w]!=null){
													if(!j1.mort && !quadriMonster[u][w].mort){
														if(vargrav>0){
															if(quadriMonster[u][w].Collision(new Rectangle((int)(j1.x+10),(int)(j1.y+(j1.height-1)),(int)(j1.width-20),1))){
																quadriMonster[u][w].mort=true;
																j1.dy=-5;
																quadriMonster[u][w].dy=-5;
																l1.tue=l1.tue+1;
															}
														}
														if(vargrav<0){
															if(quadriMonster[u][w].Collision(new Rectangle((int)(j1.x+10),(int)(j1.y),(int)(j1.width-20),1))){
																quadriMonster[u][w].mort=true;
																j1.dy=5;
																quadriMonster[u][w].dy=5;
																l1.tue=l1.tue+1;
															}
															
														}
													}
												}
											}
										}																		
								}
								
							
					//----------------------------\\
								
						}
			
						
				//_____________Collision BONUS Changement de Niveau____________\\
						for(int u=0;u<nbLignes;u++){					
							for(int w=0;w<nbCol;w++){
								if(quadriBonus[u][w]!=null){
									if(!j1.mort && !quadriBonus[u][w].mort){
										if(j1.Collision(new Rectangle((int)(quadriBonus[u][w].x),(int)(quadriBonus[u][w].y),(int)(quadriBonus[u][w].width),(int)quadriBonus[u][w].height)))
										{
											quadriBonus[u][w].mort=true;
                                            quadriBonus[u][w].img=null;
											j1.nbvies++;


											if(l1.level<4){
											chargeLeNiveau(l1.level+1);
											}

										}
									}
								}
							}
						}
						
						
		//______________________________collision mario/monstres (sur les cot�s)______________________________\\
						
						//Tableau de Monstres \\
						for(int u=0;u<nbLignes;u++){					
							for(int w=0;w<nbCol;w++){
								if(quadriMonster[u][w]!=null){
									if(!j1.mort && !quadriMonster[u][w].mort){
										//collision droite\\
										if(quadriMonster[u][w].Collision(new Rectangle((int)(j1.x+(j1.width-3)),(int)(j1.y+5),5,(int)(j1.height-10))))
										{
											j1.nbvies=j1.nbvies-1;
											j1.dy=-15;
										}
										//collision gauche\\
										if(quadriMonster[u][w].Collision(new Rectangle((int)(j1.x),(int)(j1.y+5),5,(int)(j1.height-10))))
										{
											j1.nbvies=j1.nbvies-1;
											j1.dy=-15;
										}
									}
								}
							}
						}
						
						
						
			//_________________________________________________________________________________________\\			
						
						
						
						
//____________________________Code permetant de deplacer Mario (gauche et droite_____________________________________\\
				//_____________________ D�placement a Gauche _________________________\\	
						if(PressGauche && j1.PeuxTournerAGauche && !j1.mort)
						{
							j1.PeuxTournerADroite=true;
							for(int i=0;i<nbLignes;i++){					
								for(int j=0;j<nbCol;j++){
									//Decor Bouge\\
									if(!j1.mort && quadriDecor[i][j]!=null){
										quadriDecor[i][j].Bouger(20,0);
										quadriDecor[i][j].prevX=quadriDecor[i][j].x;
									}
									//Monstre bouge\\
									if(!j1.mort && quadriMonster[i][j]!=null){		
										quadriMonster[i][j].Bouger(20,0);
									}
									//bonus bouge\\
									if(!j1.mort && quadriBonus[i][j]!=null){
										quadriBonus[i][j].Bouger(20,0);
									}
								}
							}
							
							
						}
				//_____________________ D�placement a droite _________________________\\		
						if(PressDroit && j1.PeuxTournerADroite && !j1.mort)
						{
						j1.PeuxTournerAGauche=true;
						
							
							
							//monstre bouge\\
							for(int i=0;i<nbLignes;i++){					
								for(int j=0;j<nbCol;j++){
									//Decor Bouge\\
									if(!j1.mort && quadriDecor[i][j]!=null){	
										quadriDecor[i][j].Bouger(-20,0);
										quadriDecor[i][j].prevX=quadriDecor[i][j].x;
									}
									//monstre bouge\\
									if(!j1.mort && quadriMonster[i][j]!=null){
										quadriMonster[i][j].Bouger(-20,0);
									}
									//bonus bouge\\
									if(!j1.mort && quadriBonus[i][j]!=null){
										quadriBonus[i][j].Bouger(-20,0);
									}
								}
							}
							
							
						}
						
					}
					catch(Exception ex)
					{
						System.out.println("Erreur dans le thread: "+ex);
					}
				repaint();
				}
			}
			
//___________________________________________fonction � cot� du thread__________________________________\\			
			public void start(){
				if (processusJeu==null)
				{
					processusJeu=new Thread(this);
					processusJeu.start();
				}
			}
			
			public void interrupt()
			{
				if (processusJeu!=null)
				{
					processusJeu.interrupt();
					processusJeu=null;
				}
			}
			
			public void destroy(){
				if(processusJeu!=null)
				{
					processusJeu.interrupt();
					processusJeu=null;
				}
			}
			
	//________________________________________Gravit�________________________________________\\
	public void gravite(){
		
		//gravit� joueur
		if(!j1.ToucheLeSol)
		{
			j1.dy+=3*vargrav;
			j1.y+=j1.dy;
			
			if(j1.dy>0){
				j1.DoubleSaut=2;
			}
		}
		else if(j1.ToucheLeSol)
		{
			j1.dy=0;
		}
		
	//Gravit� Monstre\\
			
		for(int i=0;i<nbLignes;i++){					
			for(int j=0;j<nbCol;j++){
				if(quadriMonster[i][j]!=null){
					
					//quadriMonster[i][j]
					if(!quadriMonster[i][j].ToucheLeSol)
					{
						quadriMonster[i][j].dy+=2*vargrav;
						quadriMonster[i][j].y+=quadriMonster[i][j].dy;
					}
					else if(quadriMonster[i][j].ToucheLeSol)
					{
						quadriMonster[i][j].dy=0;
					}

				}
			}
		}
		
			
	}
//_____________________________Perd/Mort Monstre/Mort Mario ______________________________________\\	
	public void perdu(){
		if(j1.mort){
			fond=fondPerdu;
		}
		
	}
	
}
