package mario;

import java.awt.*;

public class Joueur extends Rectangle{
	public String nom;
	public double x,y,height,width;
	
	public int dx,dy;
	public int nbvies, DoubleSaut;
	public Image img;
	public boolean ToucheLeSol, mort;
	boolean PeuxTournerAGauche=true, PeuxTournerADroite=true;
	public double vitesseSaut;
	public Rectangle R;


	public Joueur(String param1,Image param2)
	{
		nom=param1;
		x=550;
		y=500;
		width=40;
		height=60;
		dx=0;
		dy=0;
		nbvies=1;
		mort=false;
		ToucheLeSol=false;
		img=param2;
		vitesseSaut=0;
		DoubleSaut=0;
		
	}
	
	public void Dessiner(Graphics g)
	{
		g.drawImage(img,(int)x,(int)y,40,60, null);
		
	}
	
	public void Bouger(int dx, int dy)
	{
		x=x+dx;
		y=y+dy;
	}
	
	public boolean Collision(Rectangle r){
		R= new Rectangle((int)x,(int)y,(int)width,(int)height);
        return R.intersects(r);
	}
	
	public void DoubleSaut(){
		if(DoubleSaut==2 && ToucheLeSol){
			DoubleSaut=0;
		}
	}
	
	public void Meurt(){
		if(y>=1000 || y<-100){
			mort=true;
		}
		if(nbvies<=0){
			mort=true;
		}
		if(y>=1000){
			mort=true;
			dx=0;
			dy=0;
			
		}
	}
}