package mario;

import java.awt.*;

public class Decor extends Rectangle{
	public String nom;
	public double x,y,height,width;
	public double prevX, prevY;
	public double dx,dy;
	public Image img;
	public Rectangle R;


	public Decor(String param1,Image param2, int xpos, int ypos)
	{
		nom=param1;
		x=xpos;
		y=ypos;
		width=70;
		height=60;
		dx=0;
		dy=0;
		img=param2;
		
	}
	
	public void Dessiner(Graphics g)
	{
		g.drawImage(img,(int)x,(int)y,(int)width,(int)height, null);
	}
	public void Bouger(int para1, int para2)
	{
		x=x+para1;
		y=y+para2;
	}

	
	public boolean Collision(Rectangle r){
		R= new Rectangle((int)x,(int)y,(int)width,(int)height);
        return R.intersects(r);
	}
}
