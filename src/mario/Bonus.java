package mario;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

public class Bonus extends Rectangle{
	public String nom;
	public double x,y,height,width;
	public double dx,dy;
	public Image img;
	public boolean ToucheLeSol;
	boolean  mort;
	public Rectangle R;


	public Bonus(String param1,Image param2 , int param3, int param4)
	{
		nom=param1;
		x=param3;
		y=param4;
		width=30;
		height=30;
		dx=0;
		dy=0;
		ToucheLeSol=false;
		img=param2;
		mort=false;
	}
	
	public void Dessiner(Graphics g)
	{
		g.drawImage(img,(int)x,(int)y,30,30, null);
	}
	
	public void Bouger(int dx, int dy)
	{
		x=x+dx;
		y=y+dy;
	}
	
	
	public boolean Collision(Rectangle r){
		R= new Rectangle((int)x,(int)y,(int)width,(int)height);
        return R.intersects(r);
	}

}